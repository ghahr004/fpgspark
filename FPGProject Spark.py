from pyspark.sql import SparkSession

spark = SparkSession.builder.appName('FPGSpark').getOrCreate()

source = '/Users/peymanghahremani/Dropbox/Programing and Thesis/FrontlineCSV/Data Set.csv'

data = spark.read.csv(source, inferSchema=True, header = True)

data.printSchema()

data.show()

# Since 4 of the columns are categorical we convert them to integers.


from pyspark.ml import Pipeline
from pyspark.ml.feature import StringIndexer

indexers = [StringIndexer(inputCol=column, outputCol=column+"_index").fit(data) for column in list(set(data.columns)-set(['Score','Observation Time in Seconds'])) ]


pipeline = Pipeline(stages=indexers)
df_r = pipeline.fit(data).transform(data)

drop_list = ['Hotel Name', 'Observation Category', 'Performance Leader(Coach)','Recommended Coaching']

data_encoded = df_r.select([column for column in df_r.columns if column not in drop_list])

data_encoded.printSchema()

from pyspark.sql.functions import *


# For simplicity, we rename the columns of the data_encoded dataframe as follows.
# 'Recommended Coachong_index' as 'label'   
# 'Hotel Name_index' as 'HNI'
# 'Observation Category_index' as 'OCI'
# 'Performance Leader(Coach)_index' as 'CI'
# 'Score' as 'Score' no change to this column title
# 'Observation Time in Seconds' as 'OTSI'

clean_data = data_encoded.select(col('Recommended Coaching_index').alias('label'), 
              col('Hotel Name_index').alias('HNI'),
              col('Observation Category_index').alias('OCI'),
              col("Performance Leader(Coach)_index").alias("CI"),
              col('Score').alias('Score'),
              col("Observation Time in Seconds").alias("OTSI"))

clean_data.show()

from pyspark.ml.feature import (VectorAssembler, VectorIndexer, OneHotEncoder, StringIndexer)

# We prepare the dataframe into two columns, label and features.

assembler = VectorAssembler(inputCols=['HNI', 'OCI', 'CI', 'Score', 'OTSI'], outputCol='features')

clean_data_modified = assembler.transform(clean_data)

clean_data_modified.show()

drop_list = ['HNI', 'OCI', 'CI','Score', 'OTSI']

label_feat = clean_data_modified.select([column for column in clean_data_modified.columns if column not in drop_list])

label_feat.show()


# Now we have a data frame which is consisted of one label 
# column and one feature column therefore
# we can use Spark ML libraries and evaluate their accuracy.

from pyspark.sql import DataFrame
from pyspark import SparkContext, SQLContext
from pyspark.ml import Pipeline
from pyspark.ml.classification import RandomForestClassifier, DecisionTreeClassifier
from pyspark.ml.feature import StringIndexer, VectorIndexer
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

# Split the data into training and test sets (30% held out for testing)
(trainingData, testData) = label_feat.randomSplit([0.7, 0.3])

# Train a Random Forest model.
rf = RandomForestClassifier(labelCol="label", featuresCol="features", numTrees=12,  maxDepth=10)
dt = DecisionTreeClassifier(labelCol='label', featuresCol='features')

# Chain RF in a Pipeline
# Chain DT in Pipeline
pipeline = Pipeline(stages=[rf])
pipeline1 = Pipeline(stages=[dt])

# Train model.
model = pipeline.fit(trainingData)
model1 = pipeline1.fit(trainingData)

# Make predictions.
predictions = model.transform(testData)
predictions1 = model1.transform(testData)

print predictions

predictions.show()

predictions1.show()

from pyspark.ml.evaluation import MulticlassClassificationEvaluator
my_multi_eval = MulticlassClassificationEvaluator(labelCol='label')

print 'ACCURACY OF RANDOM FOREST:  '
my_multi_eval.evaluate(predictions)


print 'ACCURACY OF DECISION TREE: '
my_multi_eval.evaluate(predictions1)

